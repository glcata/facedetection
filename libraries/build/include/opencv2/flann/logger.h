#include <iostream>
#include <fstream>
#include <queue>
#include <map>
#include <iterator>
#include <algorithm>

const int UniqueSymbols = 256;
unsigned char* SampleString;

typedef std::vector<bool> HuffCode;
typedef std::map<char, HuffCode> HuffCodeMap;

class INode
{
public:
	const int f;

	virtual ~INode() {}

protected:
	INode(int f) : f(f) {}
};

class InternalNode : public INode
{
public:
	INode *const left;
	INode *const right;

	InternalNode(INode* c0, INode* c1) : INode(c0->f + c1->f), left(c0), right(c1) {}
	~InternalNode()
	{
		delete left;
		delete right;
	}
};

class LeafNode : public INode
{
public:
	const char c;

	LeafNode(int f, char c) : INode(f), c(c) {}
};

struct NodeCmp
{
	bool operator()(const INode* lhs, const INode* rhs) const { return lhs->f > rhs->f; }
};

INode* BuildTree(const int(&frequencies)[UniqueSymbols])
{
	std::priority_queue<INode*, std::vector<INode*>, NodeCmp> trees;

	for (int i = 0; i < UniqueSymbols; ++i)
	{
		if (frequencies[i] != 0)
			trees.push(new LeafNode(frequencies[i], (char)i));
	}
	while (trees.size() > 1)
	{
		INode* childR = trees.top();
		trees.pop();

		INode* childL = trees.top();
		trees.pop();

		INode* parent = new InternalNode(childR, childL);
		trees.push(parent);
	}
	return trees.top();
}

void GenerateCodes(const INode* node, const HuffCode& prefix, HuffCodeMap& outCodes)
{
	if (const LeafNode* lf = dynamic_cast<const LeafNode*>(node))
	{
		outCodes[lf->c] = prefix;
	}
	else if (const InternalNode* in = dynamic_cast<const InternalNode*>(node))
	{
		HuffCode leftPrefix = prefix;
		leftPrefix.push_back(false);
		GenerateCodes(in->left, leftPrefix, outCodes);

		HuffCode rightPrefix = prefix;
		rightPrefix.push_back(true);
		GenerateCodes(in->right, rightPrefix, outCodes);
	}
}



int main()
{
	// Build frequency table
	std::ifstream file("test.txt", std::ios::in | std::ios::binary | std::ios::ate);
	std::streampos size;
	if (file.is_open())
	{
		size = file.tellg();
		SampleString = new unsigned char[size];
		file.seekg(0, std::ios::beg);
		file.read((char*)SampleString, size);
		file.close();
	}
	int frequencies[UniqueSymbols] = { 0 };
	unsigned char* ptr = SampleString;
	for (unsigned long int i = 0; i < size; ++i)//while (*ptr != '\0')
	{
		++frequencies[*ptr++];
	}

	INode* root = BuildTree(frequencies);
	HuffCodeMap codes;
	GenerateCodes(root, HuffCode(), codes);
	delete root;

	for (int i = 0; i < UniqueSymbols; ++i)
	{
		if (frequencies[i] != 0)
		{
			double prob = frequencies[i] * 100.0f / (double)size;
			std::cout << i << " " << (unsigned char)i << " " << frequencies[i] << " " << prob << "%" << std::endl;

		}
	}
	double lm = 0;
	int qq;
	FILE* f;
	fopen_s(&f, "compresie.txt", "w");

	for (HuffCodeMap::const_iterator it = codes.begin(); it != codes.end(); ++it)
	{
		std::cout << ((int)it->first) << " " << it->first << " ";

		//std::copy(it->second.begin(), it->second.end(), std::ostream_iterator<bool>(std::cout));
		bool a[20];
		std::copy(it->second.begin(), it->second.end(), a);
		for (int i = 0; i < it->second.end() - it->second.begin(); ++i)
		{
			std::cout << a[i];
			//fprintf(f, "%d",a[i]);
		}
		// fprintf(f, " ");
		std::cout << " " << it->second.end() - it->second.begin();//<<  std::endl;
		if (it->first < 0)
			qq = it->first + 256;
		else
			qq = it->first;
		lm += (it->second.end() - it->second.begin()) * frequencies[qq] * 100.0f / (double)size;
		std::cout << " lm: " << lm << " end-begin: " << it->second.end() - it->second.begin() << " freq: " << frequencies[qq] << " size: " << (double)size << std::endl;
	}
	std::cout << std::endl << "Lungimea medie este: " << lm << std::endl;

	for (int i = 0; i < size; ++i)
	{
		HuffCodeMap::iterator it = codes.begin();
		while ((char)it->first != (char)SampleString[i])
			it++;
		bool a[20];
		std::copy(it->second.begin(), it->second.end(), a);
		for (int i = 0; i < it->second.end() - it->second.begin(); ++i)
		{
			//std::cout << a[i];
			fprintf(f, "%d", a[i]);
		}
		//fprintf(f," ");
	}
	fclose(f);
	system("pause");
	return 0;
}